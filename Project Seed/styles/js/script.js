﻿
var dismissComponents = function(){
	$('button').fadeOut('slow');
	$('.img-thumbnail').fadeOut('slow');
l};

var save = function(){
	$('button').fadeIn('slow');
};

var cancel = function(){
	clearStorage();
	dismissComponents();
};

var clearStorage = function(exceptions){
  var storage = localStorage
  var keys = [];
  var exceptions = [].concat(exceptions) //prevent undefined

  //get storage keys
  $.each(localStorage, function(key, val) {
    keys.push(key);
  });

  //loop through keys
  for( i=0; i<keys.length; i++ ){
    var key = keys[i]
    var deleteItem = true
    //check if key excluded
    for( j=0; j<exceptions.length; j++ ){
      var exception = exceptions[j];
      if( key == exception ) deleteItem = false;
    }
    //delete key
    if( deleteItem ){
      localStorage.removeItem(key)
    }
  }
}

$(document).ready(function(){
	$('button').fadeOut('0');
	/*$('input[type="file"]').focus(function(){
		if($('input[type="file"]').val != ''){
			$('button').fadeIn(3000);
		}
	});*/
	save;
	cancel;
	clearStorage;
});


function handleFileSelect(evt) {
var files = evt.target.files; // FileList object

// Loop through the FileList and render image files as thumbnails.
for (var i = 0, f; f = files[i]; i++) {

  // Only process image files.
  if (!f.type.match('image.*')) {
    continue;
  }

  var reader = new FileReader();

  // Closure to capture the file information.
  reader.onload = (function(theFile) {
    return function(e) {
      // Render thumbnail.
      var span = document.createElement('span');
      span.innerHTML = ['<img class="img-thumbnail" width="200" height="150" src="', e.target.result,
                        '" title="', escape(theFile.name), '"/>'].join('');
        
      document.getElementById('list').insertBefore(span, null);
      localStorage.setItem('img', e.target.result);
    };
  })(f);

  // Read in the image file as a data URL.
  reader.readAsDataURL(f);
}
	save();
}

document.getElementById('files').addEventListener('change', handleFileSelect, false);

if(localStorage.img) { 

     var span = document.createElement('span');
      span.innerHTML += ['<img class="img-thumbnail" width="200" height="150" src="', localStorage.img,
                        '" title="test"/>'].join('');

      document.getElementById('list').insertBefore(span, null);
      save();
}



